const prompt = require('prompt-sync')();

var judul = "-Sini Coffee-";
var barang = "\nMenu :\nDaily-Koffie \nKopi Hitam\nCappucino\nKopi Latte \n\nNon-Koffie\nChocolate\nCookies and Cream\nMatcha Latte\n\nTea Selection \nMilk Tea\nLemon Tea\nLychee Tea";
console.log(judul);
console.log(barang);

// membuat array untuk menyimpan kode barang, nama barang, dan harga
let daftarBarang = [
  { nama: "Kopi Hitam", harga: 13000 },
  { nama: "Cappucino", harga: 15000 },
  { nama: "Kopi Latte", harga: 15000 },
  { nama: "Chocolate", harga: 15000 },
  { nama: "Cookies and Cream", harga: 18000 },
  { nama: "Matcha Latte", harga: 18000 },
  { nama: "Milk Tea", harga: 13000 },
  { nama: "Lemon Tea", harga: 13000 },
  { nama: "Lychee Tea", harga: 13000 },
];

// membuat variabel untuk menyimpan total harga
let totalHarga = 0;

// mengulangi input nama barang sampai user memilih berhenti
while (true) {
  let namaBarang = prompt("Masukkan Menu Pesanan Anda: ");
  if (namaBarang.toLowerCase() === "itu aja") {
    break;
  }
  
  let barang = daftarBarang.find((item) => item.nama === namaBarang);
  
  if (barang) {
    let jumlah = parseInt(prompt("Masukkan Jumlah Pesanan: "));
    totalHarga += barang.harga * jumlah;
    console.log("Barang: " + barang.nama);
    console.log("Harga: " + barang.harga);
    console.log("Jumlah: " + jumlah);
  } else {
    console.log("Menu Tidak Ada. Silahkan pilih yang lain.");
  }
}

// menampilkan total harga dan meminta input uang pembayaran
console.log("Total Harga: " + totalHarga);
let pembayaran = parseInt(prompt("Masukkan Jumlah Uang Anda: "));

// mengulangi input uang pembayaran sampai jumlah pembayaran cukup
while (pembayaran < totalHarga) {
  console.log("Uang anda tidak cukup. Silahkan coba lagi.");
  pembayaran = parseInt(prompt("Masukkan Uang Pembayaran: "));
}

// menghitung kembalian dan menampilkan informasi pembayaran dan kembalian
let kembalian = pembayaran - totalHarga;
console.log("Uang Pembayaran: " + pembayaran);
console.log("Kembalian: " + kembalian);
